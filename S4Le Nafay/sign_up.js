var signUpViewModel = {
	date: ko.observable().extend({ required: true}),
	gender: ko.observable(),
    maritalStatus: ko.observable("single"),
    mobileNumber: ko.observable().extend({ number: {message: "Please give a valid Mobile Number"}}),
    privacyTag: ko.observable("default"),
    userName: ko.observable().extend({required: true,pattern: {message:"Please Enter a alphanumeric username" ,params:"^[A-z 0-9].$"}} ),
    identity: ko.observable("actual"),
    flatNumber: ko.observable(),
    streetNumber: ko.observable(),
    availableCountry: ['India','Srilanka','USA','UAE'],
    country: ko.observable().extend({required: true}),
    availableState: ['AP','Kerala','Maharashtra','TN','Punjab'],
    state: ko.observable(),
    availableCity: ['Hyderabad','chennai','banglore','mumbai'],
    city: ko.observable(),
    area: ko.observable(),
    pincode: ko.observable().extend({number: true,maxlength: {message: "Please give valid pincode",params: 6}}),
    availableProfession: ['Student','Engineer','Teacher','Doctor'],
    profession: ko.observable().extend({required: {message: "please select a suitable profession"}}),
    institution: ko.observable(),
    job: ko.observable(),
    signup_flag: ko.observable(),
    email_flag: ko.observable("newsletter"),
    terms_condition: ko.observable().extend({required: true})
};


