describe("pull_addresses", function() {
	var server;
	beforeEach(function() {
		server = sinon.fakeServer.create();
	});
	
	afterEach(function() {
		server.restore();
	});
	
	it("returns a list of addresses", function() {
		server.respondWith("GET", "/addresses", '[ { "street" : "mehdipatnam", "city" : "hyderabad" }, { "street" : "sahethnagar", "city" : "karimnagar" } ]');

		var callback = sinon.spy();

		pull_addresses(callback);

		server.respond();		
	 	expect(callback.calledOnce).toBeTruthy();	 	
	 	expect(callback.args[0].length).toBe(1); //no of arguments
		expect(callback.args[0][0].length).toBe(2); // argument is an array of 2 addresses
		expect(callback.args[0][0][0].street).toBe("mehdipatnam");
	});
});