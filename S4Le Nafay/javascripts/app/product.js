
// Custom Bindings
ko.bindingHandlers.addOnEnter = {
    init: function(element, valueAccessor) {
        var value = valueAccessor();
        $(element).keypress(function(event) {
            var keyCode = (event.which ? event.which : event.keyCode);
            if (keyCode === 13) {
                value.call(ko.dataFor(this));
                return false;
            }
            return true;
        });
    },
    update: function(element, valueAccesor) {

    }
};


$(document).ready(function() {
	
	ko.validation.configure({
		registerExtenders: true,
		insertMessages: true,
		messagesOnModified: true,
		parseInputAttributes: true,
		errorMessageClass: 'errorMessage'
	});


	var productDetailsViewModel = function() {

    var self = this;

		self.brandName = ko.observable().extend({required: true});
		self.modelName = ko.observable().extend({required: true});
		self.category = ko.observable();
		self.subCategory = ko.observable();
    self.tagName = ko.observable();

		self.productTags = ko.observableArray([]);
    self.tempTagName = ko.observable();

		self.productSerialNumber = ko.observable().extend({required: true, minLength: 5});
		self.productItemNumber = ko.observable().extend({required: true, minLength: 5});
		self.productWebsite = ko.observable().extend({required: true});
		self.productWidth = ko.observable().extend({required: true});
		self.productHeight = ko.observable().extend({required: true});
		self.productLength = ko.observable().extend({required: true});     
		self.productWeight = ko.observable().extend({required: true});
    
		self.removeTag = function() { 

      self.productTags.remove(this);

		};

		self.addTag = function() {
    	
      self.productTags.push( {name : self.tagName()} );   
      self.tagName("");  

		};

 };

 var owner = function() {
  
      var self = this;

      self.name = ko.observable();
      self.startPeriod = ko.observable();
      self.endPeriod = ko.observable();

    };

    var purchaseDetailsViewModel = function() {
   
       var self = this ;
     
       self.merchantName = ko.observable();
       self.location = ko.observable();      
       self.chosenState = ko.observable();
       self.purchaseDate = ko.observable();
       self.purchasePrice = ko.observable();
       self.productWarrant =  ko.observable();       

       self.totalDays = ko.computed(function() {
          
          return Math.round((new Date() - new Date(this.purchaseDate())) / (1000 * 24 * 60 * 60));  
          
       }, this);     


       self.yearsUsed = ko.computed(function() {

           return Math.round(this.totalDays() / 365);

       }, this);

       self.monthsUsed = ko.computed(function() {
  
          return Math.round((this.totalDays() % 365) / 30);

       }, this);
          
       self.daysUsed = ko.computed(function() {
             
          return Math.round((this.totalDays() % 365 ) % 30);   

       }, this);       

       self.productWarranty = ko.observable(0);

       self.warrantyEndsOn = ko.computed(function() {
       
          if(this.purchaseDate()) {
          var purchaseDate = new Date(this.purchaseDate());
          return purchaseDate.getMonth() + 1 + "/" + purchaseDate.getDate() + "/" + 
                                  (purchaseDate.getFullYear() + parseInt(this.productWarranty()));
          }
       }, this);


       self.anyExtendedWarranty = ko.observable();
       self.warrantyName = ko.observable();
       self.warrantyTime = ko.observable();
             
       self.anyLoan = ko.observable();
       self.loanAmount = ko.observable();
       self.lender = ko.observable();
       self.loanCleared = ko.observable();
       self.balanceLoan = ko.observable();

       self.anyTransfers = ko.observable();      
       self.previousOwners = ko.observableArray([new owner()]);

       self.addOwner = function() {

         self.previousOwners.push(new owner());

       };

       self.removeOwner = function() {

         self.previousOwners.remove(this);

       };     
                
   };
 
   var service = function() {
  
      var self = this;

      self.serviceType = ko.observable();
      self.serviceTitle = ko.observable();
      self.serviceDesc = ko.observable();

   };

   var serviceDetailsViewModel = function() {

     var self = this;

     self.anyRepairs = ko.observable();    
     self.services = ko.observableArray([new service()]);
     self.addService = function() {
      
     self.services.push(new service());

     }; 

     self.removeService = function() {

      self.services.remove(this);

     };        

   };

   var additionalDetailsViewModel = function() {

     var self = this;   
     self.additionalDesc = ko.observable(); 

   };   
 
   ko.applyBindings(new productDetailsViewModel(), $("#product-details")[0]);
   ko.applyBindings(new purchaseDetailsViewModel(), $("#purchase-details")[0]);
   ko.applyBindings(new serviceDetailsViewModel(), $("#service-details")[0]);
   ko.applyBindings(new additionalDetailsViewModel(), $("#additional-details")[0]); 
 
   
  
});

/*

	$('#submit').click(function() {
		
		// Validate productForm first.
		
		alert('Screen in progress.');
	});
	
	$('#productForm').validate(
	{
		rules: {
			brandname: {
				minlength: 5,
				required: true
			},
			modelname: {
				minlength: 5,
				required: true
			},
			producttag: {
				minlength: 5,
				required: true
			},
			productserialnumber: {
				minlength:5,
				required: true
			},
			highlight: function(label) {
				    $(label).closest('.control-group').addClass('error');
				  },
			success: function(label) {
				    label
				      .text('OK!').addClass('valid')
				      .closest('.control-group').addClass('success');
			}
		});
*/
   

  

     